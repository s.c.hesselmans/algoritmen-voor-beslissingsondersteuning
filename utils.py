import sys
from typing import Set

import numpy


class Vertex:
    neighbours: "Set[Vertex]"

    def __init__(self):
        self.neighbours = set()


def parse_cinema_layout(file):
    n = int(file.readline())
    m = int(file.readline())
    rows = numpy.empty((n, m), int)
    for j in range(n):
        line = file.readline()
        for i in range(m):
            rows[(j, i)] = int(line[i])
    return n, m, rows


def parse_input_offline(path):
    with open(path, 'r') as file:
        n, m, rows = parse_cinema_layout(file)
        line = file.readline()
        groups = line.split()
        groups = [int(group) for group in groups]

        if len(file.readline().split()) > 0:
            sys.exit(-1)

        return n, m, rows, groups


def parse_input_online(path):
    with open(path, 'r') as file:
        n, m, rows = parse_cinema_layout(file)
        groups = list()
        line = file.readline()

        if len(line.split()) > 1:
            sys.exit(-1)

        while not line.startswith('0'):
            groups.append(line)
            line = file.readline()
        groups = [int(group) for group in groups]
        return n, m, rows, groups


def get_conflicts(j, i, n, m, g=0):
    """
    :param j: row coordinate of the first person in the group
    :param i: column coordinate of the first person in the group
    :param n: number of rows in the cinema
    :param m: number of columns in the cinema
    :param g: index of the group size
    :return: list of conflicts [(conflict_j, conflict_i), ...]
    """
    conflicts = list()

    # Add the nearby chairs in the row above the group to the conflicts
    conflict_j = j - 1
    if 0 <= conflict_j:
        for conflict_i in range(i - 1, (i + g + 1) + 1):
            if 0 <= conflict_i < m:
                conflicts.append((conflict_j, conflict_i))

    # Add the nearby chairs to the side of the group to the conflicts
    conflict_j = j
    for conflict_i in (range(i - 2, i)):
        if 0 <= conflict_i < m:
            conflicts.append((conflict_j, conflict_i))

    for conflict_i in (range(i + g + 1, (i + g + 2) + 1)):
        if 0 <= conflict_i < m:
            conflicts.append((conflict_j, conflict_i))

    # Add the nearby chairs in the row below the group to the conflicts
    conflict_j = j + 1
    if conflict_j < n:
        for conflict_i in range(i - 1, (i + g + 1) + 1):
            if 0 <= conflict_i < m:
                conflicts.append((conflict_j, conflict_i))

    return conflicts


def get_score(rows):
    score = 0
    for row in rows:
        for chair in row:
            if chair == 2:
                score += 1
    return score


def print_score(rows):
    print(str(get_score(rows)))


def print_groups(groups):
    print(' '.join([str(group) for group in groups]))


def print_rows(rows):
    for row in rows:
        print(''.join([str(chair) for chair in row]))


def preprocess(n: int, m: int, rows: numpy.ndarray):
    """
    Split the cinema into independent sub problems
    :param n:
    :param m:
    :param rows:
    :return:
    """
    remaining_problem = rows.copy()
    sub_problems = list()

    while True:
        # start looking for a new sub problem
        sub_problem = numpy.zeros((n, m), int)
        todo = list()
        found = False

        # find the first chair in the remaining problem
        for j in range(n):
            for i in range(m):
                if remaining_problem[(j, i)] == 1:
                    todo.append((j, i))
                    found = True
                if found:
                    break
            if found:
                break

        # if no new chair is found in the remaining problem then we are done
        if not found:
            break

        # otherwise we will start searching for nearby dependant chairs
        while todo:
            (current_j, current_i) = todo.pop()
            # remove the chair from the remaining problem
            remaining_problem[(current_j, current_i)] = 0
            # add the chair to the sub problem
            sub_problem[(current_j, current_i)] = 1
            for new_j in range(max(0, current_j - 1), min(n, current_j + 2)):
                for new_i in range(max(0, current_i - 1), min(m, current_i + 2)):
                    if remaining_problem[(new_j, new_i)] == 1:
                        todo.append((new_j, new_i))
            new_j = current_j
            new_i = current_i - 2
            if new_i >= 0 and remaining_problem[(new_j, new_i)] == 1:
                todo.append((new_j, new_i))
            new_j = current_j
            new_i = current_i + 2
            if new_i < m and remaining_problem[(new_j, new_i)] == 1:
                todo.append((new_j, new_i))
        sub_problems.append(sub_problem)

    return sub_problems
