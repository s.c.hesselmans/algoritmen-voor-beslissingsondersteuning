import numpy as np

# Returns a list of (i,j) tuples for each chair[i,j] that will be changed from 1, to occupied if placing a group on a certain location
def occupied_locations(group, location, cinema):
    occ_locations = []
    for i in range(location[0] - 1, location[0] + 2):
        for j in range(location[1] - 1, location[1] + group + 1):
            if 0 <= i < len(cinema) and 0 <= j < len(cinema[0]):
                if cinema[i, j] == 1:
                    occ_locations.append((i, j))
    if 0 <= location[0] < len(cinema) and 0 <= location[1] - 2 < len(cinema[0]):
        if cinema[location[0], location[1] - 2] == 1:
            occ_locations.append((location[0], location[1] - 2))
    if (0 <= location[0] < len(cinema) and location[1] + group + 1 >= 0 and location[
        1] + group + 1 < len(cinema[0])):
        if cinema[location[0], location[1] + group + 1] == 1:
            occ_locations.append((location[0], location[1] + group + 1))
    return occ_locations


def possible_locations(group, cinema, log=False):
    # returns a list of (x,y) tuples were the left most member of the group fits in the cinema
    occupied_chair = 2
    locations = []
    for i in range(len(cinema)):
        for j in range(len(cinema[i]) - group + 1):
            # Check if there is a person within range 1 of the group
            if occupied_chair in cinema[max(i - 1, 0):i + 2, max(j - 1, 0):j + group + 1]:
                continue
            # Check if there is a person withing range 2 on the same row
            if occupied_chair in cinema[i, max(j - 2, 0):j + group + 2]:
                continue
            # Check if there is a row of N '1's e.g. for a group of three: [1,1,1]
            if np.all(cinema[i, j:j + group] == np.full(group, 1, dtype=int)):
                locations.append((i, j))
    return locations


def score(cinema):
    return np.count_nonzero(cinema == 2)


def print_cinema_with_score(cinema):
    print('score: ' + str(score(cinema)))
    for row in cinema:
        print(''.join([str(chair) for chair in row]))
    print(" ")


def get_upper_bound_compare(cinema, groups):
    # Go over two rows, taking two vertically adjacent chairs at a time, if one of them is a 1, count it, if both are a 0, don't count it.
    x = 0
    for i in range(0, len(cinema), 2):
        if i + 1 < len(cinema):
            for j in range(len(cinema[0])):
                if cinema[i, j] == 1:
                    x += 1
                elif cinema[i + 1, j] == 1:
                    x += 1
        else:
            # Count all 1's in the last, uneven row
            x += np.count_nonzero(cinema[i] == 1)
    return min(x + score(cinema), sum([groups[i] * (i + 1) for i in range(len(groups))]))


def occupy_seats(cinema, group_size, x, y):
    # seat the group
    for i in range(x, x + group_size):
        cinema[y, i] = 2
    # mark the surrounding seats as non-usable
    if y > 0:
        for i in range(max(x - 1, 0), min(x + group_size + 1, len(cinema[0]))):
            if cinema[y - 1, i] != 0:
                cinema[y - 1, i] = 4
    if y < len(cinema) - 1:
        for i in range(max(x - 1, 0), min(x + group_size + 1, len(cinema[0]))):
            if cinema[y + 1, i] != 0:
                cinema[y + 1, i] = 4
    if x > 0:
        for i in range(max(x - 2, 0), x):
            if cinema[y, i] != 0:
                cinema[y, i] = 4
    if x + group_size < len(cinema[0]):
        for i in range(x + group_size, min(x + group_size + 2, len(cinema[0]))):
            if cinema[y, i] != 0:
                cinema[y, i] = 4
    return cinema


def solve_offline_branch_bound(cinema, groups):
    # Calculate all possible locations for each group size once:
    possible_locations_each_group = []
    for i in range(len(groups)):
        if groups[i] > 0:
            possible_locations_each_group.append(possible_locations(i + 1, cinema))
        else:
            possible_locations_each_group.append([])

    best_solution, best_score, best_groups = \
        smart_branch_and_bound(cinema, groups, possible_locations_each_group, [0, 0, 0, 0, 0, 0, 0, 0],
                               len(possible_locations_each_group) - 1, 0, cinema, 0, [], 0)

    return best_solution, best_groups

def smart_branch_and_bound(cinema, groups, possible_locations_each_group, amount_of_groups_placed, current_group,
                           current_index, best_solution, best_score, best_groups, depth):
    # If the upper bound is worse than the best score so far, stop this branch
    if get_upper_bound_compare(cinema, groups) <= best_score:
        return best_solution, best_score, best_groups

    # Loop over all possible locations for all group, starting from the highest group, and if the location is compatible with our solution so far, join them and go into recursion.
    # We avoid overlap by only allowing joins with locations further along the order. This is done by passing current_group and current_index.
    for i in range(current_group, -1, -1):
        if len(possible_locations_each_group[i]) == 0:
            continue
        # Keep track of how many groups of each size we places, so that we don't try to place more.
        if current_index != len(possible_locations_each_group[i]):
            if amount_of_groups_placed[i] + 1 > groups[i]:
                continue

        placed = False
        for j in range(current_index, len(possible_locations_each_group[i])):

            # if depth == 0:
            #     print("current_group", i, "Current location index", j, "out of", len(possible_locations_each_group[i]))

            # If compatible with the next location in the possible locations, recurse into there.
            if compatible(cinema, possible_locations_each_group[i][j], i + 1):
                if not placed:
                    placed = True
                    amount_of_groups_placed[i] += 1
                new_cinema = np.copy(cinema)
                new_cinema = occupy_seats(new_cinema, i + 1, possible_locations_each_group[i][j][1],
                                          possible_locations_each_group[i][j][0])
                new_score = score(new_cinema)
                if new_score > best_score:
                    best_solution = np.copy(new_cinema)
                    print_cinema_with_score(best_solution)
                    best_score = new_score
                    best_groups = np.copy(amount_of_groups_placed)
                best_solution, best_score, best_groups = smart_branch_and_bound(new_cinema, groups,
                                                                                possible_locations_each_group,
                                                                                np.copy(amount_of_groups_placed), i,
                                                                                j + 1,
                                                                                best_solution,
                                                                                best_score,
                                                                                best_groups, depth + 1)

        current_index = 0
    return best_solution, best_score, best_groups


def compatible(cinema, position, group_size):
    # Checks if placing a group at a certain position in a cinema is possible

    i, j = position

    # Check if there is a person within range 2 on the same row
    if np.count_nonzero(cinema[i, max(j - 2, 0):j + group_size + 2] == 2) > 0:
        return False

    # Check if there is a person within range 1 of the group
    if np.count_nonzero(cinema[max(i - 1, 0):i + 2, max(j - 1, 0):j + group_size + 1] == 2) > 0:
        return False

    # Check if there is a row of N '1's e.g. for a group of three: [1,1,1]
    if np.count_nonzero(cinema[i, j:j + group_size] == 1) == group_size:
        return True
