import random
import sys
import time
import os

import numpy as np

import ilp
import utils
import naive
import min_lost_seats
import branch_bound


def transform_groups_offline_to_online(groups):
    # Aggregates offline group format to online group format, where online group order is randomized
    online_groups = []

    for i in range(len(groups)):
        if groups[i] > 0:
            online_groups.extend([i + 1] * groups[i])

    random.shuffle(online_groups)
    return online_groups


def get_online_problem(file):
    # Tries to read the provided file as an online problem instance
    # If an offline instance is provided, then this instance is tranformed nondeterministically into an online instance
    n, m, rows, groups = None, None, None, None

    try:
        n, m, rows, groups = utils.parse_input_online(file)
    except FileNotFoundError:
        print("Error reading file: " + file)
        print_usage()
        sys.exit(-1)
    except:
        n, m, rows, groups = utils.parse_input_offline(file)
        groups = transform_groups_offline_to_online(groups)
        print("WARNING: running an online algorithm on an offline problem uses randomized logic and as such the "
              "results will be nondeterministic")

    cinema = np.array(rows)
    return cinema, groups


def transform_groups_online_to_offline(groups):
    # Aggregates online group format to offline group format
    # WARNING: this method erases all info on the ordering in the online group format
    offline_groups = [0, 0, 0, 0, 0, 0, 0, 0]

    for group in groups:
        if 1 > group or group > 8:
            print("Invalid value", group, "in online groups!")
            sys.exit(-1)

        offline_groups[group - 1] += 1

    return offline_groups


def get_offline_problem(file):
    # Tries to read the provided file as an offline problem instance
    # If an online instance is provided, then this instance is tranformed deterministically into an offline instance
    n, m, rows, groups = None, None, None, None

    try:
        n, m, rows, groups = utils.parse_input_offline(file)
    except FileNotFoundError:
        print("Error reading file: " + file)
        print_usage()
        sys.exit(-1)
    except:
        n, m, rows, groups = utils.parse_input_online(file)
        groups = transform_groups_online_to_offline(groups)

    cinema = np.array(rows)
    return cinema, groups


def run_online_with_alg(algorithm, file, i=1):
    # Run the online algorithm described by the argument algorithm on the problem instance provided by the argument file
    # If the instance is in the wrong format it is tranformed by get_online_problem
    # For benchmark purposes the argument i can be used to run multiple iterations whilst averaging the results.
    cinema, groups = get_online_problem(file)

    avg_time = 0
    result, groups_seated = None, None

    if algorithm == "min-lost-seats":
        alg_fun = min_lost_seats.solve_online_min_lost_seats
    elif algorithm == "greedy":
        alg_fun = min_lost_seats.solve_online_greedy
    else:
        print("Unknown online algorithm: " + algorithm)
        print_usage()
        sys.exit(-1)

    if alg_fun is not None:
        for _ in range(i):
            start_time = time.time()
            result, groups_seated = alg_fun(np.copy(cinema), np.copy(groups))
            avg_time += time.time() - start_time

    return result, groups_seated, avg_time / i


def run_offline_with_alg(algorithm, file, i=1):
    # Run the offline algorithm described by the argument algorithm on the problem instance provided by the argument file
    # If the instance is in the wrong format it is tranformed by get_offline_problem
    # For benchmark purposes the argument i can be used to run multiple iterations whilst averaging the results.
    cinema, groups = get_offline_problem(file)

    avg_time = 0

    if algorithm == "ilp":
        alg_fun = ilp.solve_offline
    elif algorithm == "naive":
        # TODO: fix this? don't if we wanna use this for out results
        print("WARNING: the naive alg currently returns faulty solutions!")
        alg_fun = naive.solve_offline_naive
    elif algorithm == "branch-bound":
        alg_fun = branch_bound.solve_offline_branch_bound
    else:
        print("Unknown offline algorithm: " + algorithm)
        print_usage()
        sys.exit(-1)

    if alg_fun is not None:
        for _ in range(i):
            start_time = time.time()
            result, groups_seated = alg_fun(np.copy(cinema), np.copy(groups))
            avg_time += time.time() - start_time

    return result, groups_seated, avg_time / i


def run_online_comp_with_alg(algorithm, file, i=1):
    # Run the online algorithm described by the argument algorithm on the problem instance provided by the argument file
    # This function is used to calculte the competitive ratio and running time for a given instance
    # The argument i can be used to aggregate these calculations over multiple iterations
    if algorithm == "min-lost-seats":
        alg_fun = min_lost_seats.solve_online_min_lost_seats
    elif algorithm == "greedy":
        alg_fun = min_lost_seats.solve_online_greedy
    else:
        print("Unknown online algorithm: " + algorithm)
        print_usage()
        sys.exit(-1)

    opt_result, opt_groups_seated, opt_avg_time = run_offline_with_alg("ilp", file)
    opt_score = utils.get_score(opt_result)

    print("Optimal solution")
    print_results(opt_result, opt_groups_seated)

    cinema = np.copy(opt_result)

    for y in range(len(cinema)):
        for x in range(len(cinema[0])):
            cell = cinema[y][x]
            if cell != 0 or cell != 1:
                cinema[y][x] = 1

    worst_score, worst_result, worst_group_seated, best_score, best_result, best_group_seated = None, None, None, None, None, None
    best_score = -1
    total_score = 0
    avg_time = 0

    group_combinations = []

    if alg_fun is not None:
        for k in range(i):
            start_time = time.time()
            online_groups = transform_groups_offline_to_online(np.copy(opt_groups_seated))
            group_combinations.append(online_groups)
            print("Online groups:", online_groups)
            result, groups_seated = alg_fun(np.copy(cinema), np.copy(online_groups))
            avg_time += time.time() - start_time

            score = utils.get_score(result)
            total_score += score

            if score > best_score:
                best_score = score
                best_result = result
                best_group_seated = groups_seated

            if k == 0:
                worst_score = best_score
                worst_result = best_result
                worst_group_seated = best_group_seated

            if score < worst_score:
                worst_score = score
                worst_result = result
                worst_group_seated = groups_seated

    avg_score = total_score / i

    return avg_score / opt_score, avg_time / i, worst_result, worst_group_seated, best_result, best_group_seated, group_combinations


def run_benchmark(mode, alg, iterations, until_problem, output_file):
    # Run benchmarks for multiple instances at once
    # the argument mode describes the type of benchmark.
    # online-comp runs calculations for competitive ratios, online runs the online algorithm and offline runs the offline algorithm.
    # alg describes the algorithm that has to be used for the benchmark.
    # iterations describes the amount of iterations to run on each instance.
    # until_problem describes the suffix index of the last problem to be solved.
    # output file is a string that is appended to the end of the output filename.
    if until_problem < 1:
        print("Amount of iterations has to be at least 1!")
        sys.exit(-1)

    o_file = str(int(time.time() * 1000)) + "_" + output_file + ".csv"

    result_file = open("results/" + o_file, 'x')

    try:
        if mode == "online-comp":
            result_file.write("file_name,comp_ratio,best_score,worst_score,iterations,avg_time\n")

            for problem in range(1, until_problem + 1):
                file = "TestCases/Exact" + str(problem) + ".txt"
                comp_rat, avg_time, worst_result, worst_group_seated, best_result, best_group_seated, group_comb = \
                    run_online_comp_with_alg(alg, file, iterations)

                worst_score = utils.get_score(worst_result)
                best_score = utils.get_score(best_result)

                print("File:", file, "Comp-ratio:", comp_rat, "Worst score:", worst_score, "Best score", best_score,
                      "Iterations:", iterations, "Avg time:", avg_time)
                result_file.write(
                    to_csv_string(file) + "," +
                    str(comp_rat) + "," +
                    str(worst_score) + "," +
                    str(best_score) + "," +
                    str(iterations) + "," +
                    str(avg_time) + "\n"
                )
                result_file.write(str(group_comb) + "\n")

        else:
            result_file.write("file_name,score,iterations,avg_time\n")

            for problem in range(1, until_problem + 1):
                if mode == "offline":
                    file = "TestCases/Exact" + str(problem) + ".txt"
                    result, groups_seated, avg_time = run_offline_with_alg(alg, file, iterations)
                elif mode == "online":
                    file = "TestCases/Online" + str(problem) + ".txt"
                    result, groups_seated, avg_time = run_online_with_alg(alg, file, iterations)

                score = utils.get_score(result)

                print("File:", file, "Score:", score, "Iterations:", iterations, "Avg time:", avg_time)
                result_file.write(
                    to_csv_string(file) + "," +
                    str(score) + "," +
                    str(iterations) + "," +
                    str(avg_time) + "\n"
                )
        result_file.close()
    except IndexError as err:
        result_file.close()
    except:
        result_file.close()


def to_csv_string(str):
    # utility function that transforms a string to a valid csv string
    return "\"" + str + "\""


def run_algorithm(mode, alg, file):
    # Run an algorithm to solve a problem instance
    if mode == "offline":
        result, groups_seated, avg_time = run_offline_with_alg(alg, file)
        print_results(result, groups_seated)
        return
    elif mode == "online":
        result, groups_seated, avg_time = run_online_with_alg(alg, file)
        print_results(result, groups_seated,False)
        return

    comp_rat, avg_time, worst_result, worst_group_seated, best_result, best_group_seated, group_comb= \
        run_online_comp_with_alg(alg, file)

    print_results(best_result, best_group_seated)


def print_results(result, groups_seated,offline=True):
    # Print the result of an algorithm
    if offline:
        for row in result:
            print("".join([format_cell(cell) for cell in row]))
    else:
        print(sum(groups_seated))


def format_cell(cell):
    # Utility function used to transform a cell in an array of integers to the correct output format
    if cell == 2:
        return "x"
    else:
        return str(cell)


def print_usage():
    # Prints the usage of this module in case the input is inccorect
    print(
        "USAGE: python3 main.py [arguments] file-path\n"
        "arguments:\n"
        " [-online | -online-comp] -> use online input format and online algorithm OR use offline input, find "
        "optimal solution, and then run online algorithm to estimate comp-ration\n"
        " [-a <name>]\t\t  -> algorithm to be used\n"
        " [-benchmark <i> <p> <o>] -> benchmarks until problem p, every problem i iterations, results saved in results "
        "file o\n "
        "\n"
        "offline algorithms: naive, ilp, branch-bound\n"
        "online algorithms: min-lost-seats, greedy"
    )


if __name__ == '__main__':
    # Read input arguments and call the corresponding functions to solve the problem
    alg = ""
    mode = "offline"
    benchmark = False
    iterations = 1
    until_problem = 1
    output_file = "benchmark_results"

    # Check the input
    i = 1
    while i < len(sys.argv) - 1:
        if sys.argv[i] == "-a":
            alg = sys.argv[i + 1]
            i += 2
        elif sys.argv[i] == "-online":
            if mode != "offline":
                print("Invalid use of -online!")
                print_usage()
                sys.exit(-1)

            if alg == "":
                alg = "min-lost-seats"
            mode = "online"
            i += 1
        elif sys.argv[i] == "-online-comp":
            if mode != "offline":
                print("Invalid use of -online-comp!")
                print_usage()
                sys.exit(-1)

            if alg == "":
                alg = "min-lost-seats"
            mode = "online-comp"
            i += 1
        elif sys.argv[i] == "-benchmark":
            try:
                benchmark = True
                iterations = int(sys.argv[i + 1])
                until_problem = int(sys.argv[i + 2])
                output_file = sys.argv[i + 3]
                i += 3
            except:
                print("Invalid use of -benchmark!")
                print_usage()
                sys.exit(-1)
        else:
            print("Invalid option: " + sys.argv[i])
            print_usage()
            sys.exit(-1)

    # Set defaults
    if alg == "":
        alg = "ilp"

    # Run the algorithm
    if benchmark:
        run_benchmark(mode, alg, iterations, until_problem, output_file)
    else:
        run_algorithm(mode, alg, sys.argv[len(sys.argv) - 1])
