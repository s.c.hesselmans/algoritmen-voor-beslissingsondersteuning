import numpy


def solve_offline_naive(cinema, groups):
    rows = len(cinema)
    columns = len(cinema[0])

    # print("rows: " + str(rows))
    # print("columns: " + str(rows))
    result = [[cinema[row_index][column_index] for column_index in range(columns)] for row_index in range(rows)]

    # set cinema to 2 when chair becomes unavailable
    # set result to 2 when chair is filled

    group_size_index = 7
    group_size = 8

    seated_groups = numpy.copy(groups)

    while group_size_index >= 0:
        # print('group_size_index: ' + str(group_size_index))
        # print('group_size: ' + str(group_size))

        while groups[group_size_index] > 0:
            # print('groups[group_size_index]: ' + str(groups[group_size_index]))

            found = False

            row_index = 0
            while row_index < rows:
                # print('row_index: ' + str(row_index))

                chairs = 0

                column_index = 0
                while column_index < columns:
                    # print('column_index: ' + str(column_index))

                    # if chair is available then increase chairs else reset chairs
                    if cinema[row_index][column_index] == 1:
                        chairs += 1
                    else:
                        chairs = 0

                    # print('chairs: ' + str(chairs))

                    # if enough chairs are available then place the group and go to the next group
                    if chairs == group_size:
                        found = True

                        group_row_index = row_index
                        group_start_column_index = column_index - group_size_index
                        group_end_column_index = column_index
                        group_column_index = group_start_column_index

                        while group_column_index <= group_end_column_index:
                            # print("group_row_index: " + str(group_row_index))
                            # print("group_column_index: " + str(group_column_index))

                            result[group_row_index][group_column_index] = 2

                            nearby_row_start_index = group_row_index - 1
                            nearby_row_end_index = group_row_index + 1

                            # print("nearby_row_start_index: " + str(nearby_row_start_index))
                            # print("nearby_row_end_index: " + str(nearby_row_end_index))

                            nearby_column_start_index = group_column_index - 1
                            nearby_column_end_index = group_column_index + 1

                            # print("nearby_column_start_index: " + str(nearby_column_start_index))
                            # print("nearby_column_end_index: " + str(nearby_column_end_index))

                            nearby_row_index = nearby_row_start_index
                            while nearby_row_index <= nearby_row_end_index:
                                # print("nearby_row_index: " + str(nearby_row_index))
                                if 0 <= nearby_row_index < rows:
                                    nearby_column_index = nearby_column_start_index
                                    while nearby_column_index <= nearby_column_end_index:
                                        # print("nearby_column_index: " + str(nearby_column_index))
                                        if 0 <= nearby_column_index < columns:
                                            cinema[nearby_row_index][nearby_column_index] = 2
                                        nearby_column_index += 1
                                nearby_row_index += 1

                            nearby_row_index = group_row_index
                            nearby_column_index = group_start_column_index - 2
                            # print("nearby_row_index: " + str(group_row_index))
                            # print("nearby_column_index: " + str(nearby_column_index))
                            if 0 <= nearby_column_index < columns:
                                cinema[nearby_row_index][nearby_column_index] = 2

                            nearby_row_index = group_row_index
                            nearby_column_index = group_start_column_index + 2
                            # print("nearby_row_index: " + str(group_row_index))
                            # print("nearby_column_index: " + str(nearby_column_index))
                            if 0 <= nearby_column_index < columns:
                                cinema[nearby_row_index][nearby_column_index] = 2

                            group_column_index += 1
                    if found:
                        break
                    column_index += 1
                if found:
                    break
                row_index += 1

            # print('cinema:')
            # for row in cinema:
            #   print(row)
            # print('result:')
            # for row in result:
            # print(row)

            if found:
                # print("found")
                groups[group_size_index] -= 1
            else:
                # print("not found")
                break
        group_size_index -= 1
        group_size -= 1

    for i in range(len(groups)):
        seated_groups[i] -= groups[i]

    return result, seated_groups
