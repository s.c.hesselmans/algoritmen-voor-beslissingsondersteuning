import numpy as np

#Returns number of chairs lost when group of group_size is placed at the given location
def chairs_lost(cinema, group_size, x, y):
    res = 0

    #Count seats above
    if (y > 0):
        res += np.count_nonzero(cinema[y - 1, max(x - 1, 0):x + group_size + 1] == 1)

    #Count seats below
    if (y < len(cinema) - 1):
        res += np.count_nonzero(cinema[y + 1, max(x - 1, 0):x + group_size + 1] == 1)

    #Count seats to the left
    res += np.count_nonzero(cinema[y, max(x - 2, 0):x] == 1)

    #Count seats to the right
    if (x + group_size < len(cinema[0])):
        res += np.count_nonzero(cinema[y, x + group_size:min(x + group_size + 2, len(cinema[0]))] == 1)

    return res

#Returns number of free adjacent seats (Capped at given group_size)
def free_adjacent_seats(cinema, group_size, x, y):
    res = group_size
    for i in range(group_size):
        # print(x+i,y)
        if (x + i >= len(cinema[y]) or cinema[y, x + i] != 1):
            res = i;
    return res


lostSeatMatrix = [[], [], [], [], [], [], [], []]
needsRecalc = [[], [], [], [], [], [], [], []]
lostSeatsDict = [{}, {}, {}, {}, {}, {}, {}, {}]

#Initializes precalculation matrices
def init_matrices(cinema):
    global lostSeatMatrix
    global needsRecalc
    for i in lostSeatMatrix:
        for j in range(25):
            i.append([])

    for i in range(len(cinema)):
        for j in range(len(cinema[0])):
            if (cinema[i, j] == 1):
                for re in needsRecalc:
                    re.append((i, j))

#Recalulates queued locations
def recalc_locations(cinema, group_size, locations):
    global lostSeatMatrix
    global lostSeatsDict
    global itemsRecalc
    # Read the recalculation queue for this group size
    while len(locations) > 0:
        loc = locations.pop()
        # If the location is outside of the cinema or there is no seat in this position, we dont need to calculate it
        if loc[0] < 0 or loc[1] < 0 or loc[1] >= len(cinema[0]) or loc[0] >= len(cinema) or cinema[loc[0], loc[1]] == 0:
            continue

        fas = free_adjacent_seats(cinema, group_size, loc[1], loc[0])

        # Re-calculate the lost seats if the group still fits in this position
        if (fas >= group_size):
            lostSeats = chairs_lost(cinema, group_size, loc[1], loc[0])
            if loc in lostSeatsDict[group_size - 1]:
                # If the lost seats changed for this location, add this value to the correct array
                oldLostSeats = lostSeatsDict[group_size - 1][loc]
                if (oldLostSeats != lostSeats):
                    lostSeatMatrix[group_size - 1][lostSeats].append(loc)
            # Location calculated for the first time
            else:
                lostSeatMatrix[group_size - 1][lostSeats].append(loc)
            # If the group fits we always update the dictionary
            lostSeatsDict[group_size - 1][loc] = lostSeats
        # If a group does not fit, set it dictionary value to 0
        elif loc in lostSeatsDict[group_size - 1]:
            lostSeatsDict[group_size - 1][loc] = -1


def find_least_lost_seats(cinema, group_size):
    global lostSeatMatrix
    # Recalculate queued locations
    recalc_locations(cinema, group_size, needsRecalc[group_size - 1])

    # Find first location in the matrix that is still valid
    for i in range(len(lostSeatMatrix[group_size - 1])):
        if (len(lostSeatMatrix[group_size - 1][i]) > 0):
            for _ in range(len(lostSeatMatrix[group_size - 1][i])):
                loc = lostSeatMatrix[group_size - 1][i].pop()
                #Skip location if outside the cinema or there is no seat there
                if loc[0] < 0 or loc[1] < 0 or loc[1] >= len(cinema[0]) or loc[0] >= len(cinema) or cinema[loc[0], loc[1]] == 0:
                    continue
                # If seat is still available and this queued value is still up to date, return the new location
                if (cinema[loc[0], loc[1]] == 1 and i == lostSeatsDict[group_size - 1][loc]):
                    return loc[1], loc[0]

    # There is no location possible for this group
    return -1, -1


def occupy_seats(cinema, group_size, x, y):
    # seat the group
    for i in range(x, x + group_size):
        cinema[y, i] = 2
    # mark the surrounding seats as non usable
    if (y > 0):
        for i in range(max(x - 1, 0), min(x + group_size + 1, len(cinema[0]))):
            if (cinema[y - 1, i] != 0):
                cinema[y - 1, i] = 4
    if (y < len(cinema) - 1):
        for i in range(max(x - 1, 0), min(x + group_size + 1, len(cinema[0]))):
            if (cinema[y + 1, i] != 0):
                cinema[y + 1, i] = 4
    if (x > 0):
        for i in range(max(x - 2, 0), x):
            if (cinema[y, i] != 0):
                cinema[y, i] = 4
    if (x + group_size < len(cinema[0])):
        for i in range(x + group_size, min(x + group_size + 2, len(cinema[0]))):
            if (cinema[y, i] != 0):
                cinema[y, i] = 4

    return cinema

#Simple first fit greedy algorithm
def greedy_fit(y, x, cinema, group_size):
    for i in range(y, len(cinema)):
        for j in range(x, len(cinema[0])):
            if cinema[i, j] == 1:
                fas = free_adjacent_seats(cinema, group_size, j, i)
                if fas >= group_size:
                    return j, i
                else:
                    j += fas

    return -1, -1

#Clean print cinema
def print_cinema(cinema):
    all = ""
    for y in cinema:
        res = ""
        for x in y:
            if (x == 4):
                res += "1"
            elif (x == 2):
                res += "x"
            else:
                res += str(x)
        all += res + "\n"
    return all


# Queue locations for recalculation after last group was placed
def queueRecalc(cinema, x, y, group_size):
    global needsRecalc
    # Queue for each group size
    for i in range(8):
        for j in range(group_size + 8 + i):
            x_offset = x - 4 - i + j
            needsRecalc[i].append((y, x_offset))

        for j in range(group_size + 6 + i):
            x_offset = x - 3 - i + j
            needsRecalc[i].append((y - 1, x_offset))
            needsRecalc[i].append((y + 1, x_offset))

        for j in range(group_size + 4 + i):
            x_offset = x - 2 - i + j
            needsRecalc[i].append((y - 2, x_offset))
            needsRecalc[i].append((y + 2, x_offset))



def solve_online_min_lost_seats(cinema, groups):
    cinema = np.array(cinema)

    #Initialize matrices for precalculation
    init_matrices(cinema)

    peoplefitted = 0
    peopleNotFitted = 0
    groupsfitted = []

    for group in groups:
        x, y = find_least_lost_seats(cinema, group)
        if (x >= 0 and y >= 0):
            #Printing for required output
            print(x+1,y+1)
            peoplefitted += group
            groupsfitted.append(group)

            #Place group in stored cinema
            cinema = occupy_seats(cinema, group, x, y)

            #Queue locations for recalc
            queueRecalc(cinema, x, y, group)
        else:
            #Printing for required output
            print(0,0)
            peopleNotFitted += group

    return cinema, groupsfitted


def solve_online_greedy(cinema, groups):
    cinema = np.array(cinema)

    groups_fitted = []

    #Remember last searched row for each groupsize
    last_y_group = [0, 0, 0, 0, 0, 0, 0, 0]

    # i = 0
    for group in groups:
        x, y = greedy_fit(last_y_group[group - 1], 0, cinema, group)

        if x >= 0 and y >= 0:
            #Remember last searched row for each groupsize
            last_y_group[group - 1] = y

            groups_fitted.append(group)
            cinema = occupy_seats(cinema, group, x, y)
        else:
            #Printing for required output
            print(0,0)
    return cinema, groups_fitted
