import logging

import mip
import numpy as np
import time

import utils

logger = logging.getLogger('mip.gurobi')
logger.disabled = True

try:
    import mip.gurobi
    has_gurobi = True
except ImportError:
    has_gurobi = False

# only called if cinema[y][x] == 1
def max_possible_group_size(cinema, x, y):
    max_size = 1

    for i in range(x + 1, min(x + 8, len(cinema[y]))):
        if cinema[y][i] == 0:
            return max_size

        max_size += 1

    return max_size


def solve_offline(cinema, groups):
    n = len(cinema)
    m = len(cinema[0])

    if (has_gurobi):
        model = mip.Model(name="cinema", solver_name="gurobi")
    else:
        model = mip.Model(name="cinema")

    model.verbose = 0

    location_group_variables = np.array([[None for _ in range(m)] for _ in range(n)])
    for y in range(n):
        for x in range(m):
            if cinema[(y, x)] == 1:
                a = [(model.add_var(name="var_l{:02d}_y{:02d}_x{:02d}".format(group_size, y, x), var_type="B"), group_size)
                  for group_size in range(1, max_possible_group_size(cinema, x, y) + 1) if groups[group_size - 1] != 0]
                location_group_variables[y,x] = a

    for y in range(len(location_group_variables)):
        for x in range(len(location_group_variables[0])):
            if location_group_variables[y][x] is None:
                continue

            current_vars = location_group_variables[y][x]

            # add constraints for group_sizes on the same location
            for i in range(len(location_group_variables[y][x])):
                for j in range(i + 1, len(location_group_variables[y][x])):
                    model += current_vars[i][0] + current_vars[j][0] <= 1

            # add constraints for locations upper right of the current location
            if y > 0:
                for i in range(min(x + 1, len(location_group_variables[y])), min(x + 9, len(location_group_variables[y]))):
                    if location_group_variables[y - 1][i] is None:
                        continue

                    for cur_var in current_vars:
                        if i - x > cur_var[1]:  # out of range:
                            continue

                        for var in location_group_variables[y - 1][i]:
                            model += cur_var[0] + var[0] <= 1

            # add constraints for locations right of the current location
            for i in range(min(x + 1, len(location_group_variables[y])), min(x + 10, len(location_group_variables[y]))):
                if location_group_variables[y][i] is None:
                    continue

                for cur_var in current_vars:
                    if i - x > cur_var[1] + 1:  # out of range:
                        continue

                    for var in location_group_variables[y][i]:
                        model += cur_var[0] + var[0] <= 1

            # add constraints for locations lower and lower right of the current location
            if y < len(location_group_variables) - 1:
                for i in range(x, min(x + 9, len(location_group_variables[y]))):
                    if location_group_variables[y + 1][i] is None:
                        continue

                    for cur_var in current_vars:
                        if i - x > cur_var[1]:  # out of range:
                            continue

                        for var in location_group_variables[y + 1][i]:
                            model += cur_var[0] + var[0] <= 1

    # flatten the array of variables
    variables = list()
    for j in range(n):
        for i in range(m):
            if cinema[(j, i)] == 1:
                variables.extend(location_group_variables[(j, i)])

    model.objective = mip.maximize(mip.xsum(var[0] * var[1] for var in variables))

    for group in range(len(groups)):
        if groups[group] == 0:
            continue

        model += mip.xsum(var[0] for var in variables if var[1] == group + 1) <= groups[group]

    model.optimize()

    groups_seated = [0 for _ in range(8)]

    result = np.copy(cinema)
    for j in range(n):
        for i in range(m):
            if cinema[(j, i)] == 1:
                for group_variable in location_group_variables[(j, i)]:
                    if group_variable[0].x == 1:
                        other_j = j
                        for other_i in range(i, i + group_variable[1]):
                            result[(other_j, other_i)] = 2
                        for conflict in utils.get_conflicts(j, i, n, m, group_variable[1] - 1):
                            if result[conflict] != 0:
                                result[conflict] = 4
                        groups_seated[group_variable[1] - 1] += 1

    return result, groups_seated
