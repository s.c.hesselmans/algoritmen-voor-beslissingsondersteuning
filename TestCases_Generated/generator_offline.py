import numpy

rows = 15
columns = 15
chair_probability = 0.9
max_group_count = 20

# Generate cinema
cinema = numpy.random.choice([0, 1], (rows, columns), True, [1 - chair_probability, chair_probability])
# add horizontal path
horizontal_path = numpy.random.choice(numpy.arange(0, rows))
cinema[horizontal_path] = numpy.zeros(columns)
# add vertical path
vertical_path = numpy.random.choice(numpy.arange(0, columns - 1))
cinema[:, vertical_path:vertical_path + 2] = numpy.zeros((rows, 2))

groups = numpy.random.choice(numpy.arange(0, max_group_count), 8)

print(rows)
print(columns)

for row in cinema:
    print(''.join([str(chair) for chair in row]))

print(' '.join([str(group) for group in groups]))
