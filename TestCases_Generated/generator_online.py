import numpy

rows = 15
columns = 15
chair_probability = 0.9

# Generate cinema
cinema = numpy.random.choice([0, 1], (rows, columns), True, [1 - chair_probability, chair_probability])
# add horizontal path
horizontal_path = numpy.random.choice(numpy.arange(0, rows))
cinema[horizontal_path] = numpy.zeros(columns)
# add vertical path
vertical_path = numpy.random.choice(numpy.arange(0, columns - 1))
cinema[:, vertical_path:vertical_path + 2] = numpy.zeros((rows, 2))

groups = numpy.random.choice([1, 2, 3, 4, 5, 6, 7, 8],
                             numpy.random.choice(numpy.arange(10, 50)),
                             True,
                             [0.2, 0.2, 0.2, 0.1, 0.1, 0.1, 0.05, 0.05])

print(rows)
print(columns)

for row in cinema:
    print(''.join([str(chair) for chair in row]))

for group in groups:
    print(group)

print(0)
