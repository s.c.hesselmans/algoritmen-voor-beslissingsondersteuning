    
# Cinema Seating problem solver _Team name TODO_ 


This program aims to efficiently solve the Cinema Seating problem as define [in this document](./cinema-assignment.pdf).

## Installation

An installation of Python 3.8 is required, which can be downloaded from [here](https://www.python.org/downloads/).

The two Python libraries Numpy and MIP are used throughout the progam.
These dependencies can be installed using the Python package manager [pip](https://pip.pypa.io/en/stable/) (Bundled with Python 3.8).

#####Numpy installation command:

```bash
pip install numpy
```

#####MIP installation command:

```bash
pip install mip
```

Furthermore, the MIP library can run as an independent ILP solver, however, it can also leverage the faster commercial
ILP solver Gurobi, for which an installation guide can be found [here](https://www.gurobi.com/)

## Usage

The cli interface of the program is defined as follows:

```bash
python3 main.py [arguments] file-path

arguments:
 [-online | -online-comp] -> overwrite the default of running offline problems and algorithms
    * online:      use online input format and online algorithm
    * online-comp: use offline input, find optimal solution, and then run online algorithm to estimate comp-ration
 [-a <name>]              -> algorithm to be used
 [-benchmark <i> <p> <o>] -> benchmarks until problem p, every problem i iterations, results saved in results file o
``` 

By default the program tries to solve the problem in the file given with argument 'file-path' with an offline problem solver.
This can also be set using the '-online' argument to use online algorithms instead.

The available algorithms that can be used are the following:

Offline:
   * branch-bound
   * ilp (default)
   * naive (broken)

Online:
   * greedy
   * least-lost-seats (default)

So running a greedy algorithm can be done with the following command:

```bash
python3 main.py -online -a greedy TestCases/Online1.txt
```

Running a benchmark over 7 iterations for every test case until Exact10.txt using the ILP can be done with the 
following command:

```bash
python3 main.py [-a ilp] -benchmark 7 10 ilp-benchmark-7-iterations-until-10
```

The results of the benchmark will be saved to a newly created file in the ``/input`` folder. The name of the csv file
is the name specified in the command with the epoch prepended to the file name. 

## License
[MIT](https://choosealicense.com/licenses/mit/)


## References
- Graph Theory
    - [Tutorial](https://www.tutorialspoint.com/graph_theory/index.htm)
- Fast Local Search for the Maximum Independent Set Problem
    - [Page](https://link.springer.com/chapter/10.1007/978-3-540-68552-4_17)
    - [Paper](https://link.springer.com/content/pdf/10.1007%2F978-3-540-68552-4_17.pdf)
- An Algorithm for Finding Maximum Independent Set in a Graph
    - Seems not so serious based on journal name.
    - [Paper](https://www.researchgate.net/profile/Ahmad_Sharieh/publication/253601987_An_Algorithm_for_Finding_Maximum_Independent_Set_in_a_Graph/links/56f78db808ae38d710a1c412/An-Algorithm-for-Finding-Maximum-Independent-Set-in-a-Graph.pdf)